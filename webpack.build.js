const ExtractTextPlugin = require('extract-text-webpack-plugin');
// 要加两个插件
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const packageInfo = require('./package.json');

const outputPath = __dirname + '/output/' + packageInfo.version;
const releasePath = __dirname + '/output/release';

module.exports = {
	entry: [
		"./main.js"
	],
	output: {
		path: outputPath, // 输出到版本号目录
		filename: 'app.js'
	},
	module: {
		loaders: [
			{
				test: /\.(es6|jsx)?$/,
				exclude: /(node_modules|bower_components)/,
				loader: 'babel',
				query: {
					presets: ['es2015', 'react'],
					plugins: [
						"transform-es2015-block-scoping",
						"transform-class-properties",
						"transform-es2015-computed-properties"
					]
				}
			},
			{
				test: /\.(png|jpg|jpeg|gif|woff|woff2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
				loader: 'url-loader?limit=1000'
			},
			{
				test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
				loader: 'file'
			},
			{
				test: /\.styl$/,
				// loader: "style!css!stylus"
				loader: ExtractTextPlugin.extract('style', 'css!stylus')
			},
			{
				test: /\.css$/,
				// loader: "style!css"
				loader: ExtractTextPlugin.extract('style', 'css')
			}
		]
	},
	plugins: [
		new ExtractTextPlugin('app.css'),
		new HtmlWebpackPlugin({
			title: 'Webpack Tutorial - ver ' + packageInfo.version
		}),
		new CopyWebpackPlugin([
			// 打包出release
			{
				from: outputPath,
				to: releasePath,
				toType: 'dir'
			}
		])
	]
};
