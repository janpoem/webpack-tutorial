
var test = require('./test');
var ReactDOM = require('react-dom');
var React = require('react');
var HelloWorld = require('./hello_world.jsx');
require('./style.css');
require('./hello.styl');

var doc = document, body = doc.body;

body.onload = function() {
	var el = doc.createElement('div');
	el.style.opacity = 0;
	el.style.marginTop = '-100px';
	el.style.transitionProperty = 'opacity margin-top';
	el.style.transitionDuration = '800ms';
	el.style.transitionTimingFunction = 'cubic-bezier(0.65,-0.1, 0.24, 1.47)';
	body.appendChild(el);
	ReactDOM.render(React.createElement(HelloWorld), el);
	setTimeout(function() {
		el.style.opacity = 1;
		el.style.marginTop = 0;
	}, 1);
};