# webpack使用的简单教程

获取项目以后，请执行：

```shell
npm install
```

启动webpack-dev-server

```
webpack-dev-server --content-base ./public --hot --inline --port 8088
```

访问[http://localhost:8088/](http://localhost:8088/)查看。

webpack build

```
webpack --config webpack.build.js
```

也可以使用npm指令：

```
npm run dev   // webpack-dev-server
npm run build // 构建
```