module.exports = {
	entry: [
		"./main.js"
	],
	output: {
		path: './output',
		filename: 'app.js'
	},
	module: {
		loaders: [
			{
				test: /\.(es6|jsx)?$/,
				exclude: /(node_modules|bower_components)/,
				loader: 'babel',
				query: {
					presets: ['es2015', 'react'],
					plugins: [
						"transform-es2015-block-scoping",
						"transform-class-properties",
						"transform-es2015-computed-properties"
					]
				}
			},
			{
				test: /\.(png|jpg|jpeg|gif|(woff|woff2)?(\?v=[0-9]\.[0-9]\.[0-9])?)$/,
				loader: 'url-loader?limit=1000'
			},
			{
				test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
				loader: 'file'
			},
			{
				test: /\.styl$/,
				loader: "style!css!stylus"
			},
			{
				test: /\.css$/,
				loader: "style!css"
			}
		]
	}
};
