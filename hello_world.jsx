var React = require('react');

class HelloWorld extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			url: '',
			value: 'http://tool.oschina.net/'
		};
	}

	getUrl() {
		let url = this.state.value;
		if (/^\/\//.test(url)) {
			url = 'http:' + url;
		}
		else if (!/^https?\:\/\//i.test(url)) {
			url = 'http://' + url;
		}
		return url;
	}

	changeUrl(value) {
		this.setState({value: value});
	}

	goUrl(value, event) {
		if (event && event.preventDefault)
			event.preventDefault();
		this.setState({url: value});
	}

	renderFrame() {
		if (this.state.url) {
			return <iframe src={this.state.url} width="100%" height="500" />
		}
	}

	componentDidMount() {
		this.goUrl(this.getUrl());
	}

	render() {
		return <div>
			<h1>Hello world!!</h1>
			<form onSubmit={(e) => this.goUrl(this.getUrl(), e)}>
				<input type="text" value={this.state.value} onChange={(e) => this.changeUrl(e.target.value)}/>
				<button>Hello world</button>
			</form>
			{this.renderFrame()}
		</div>;
	}
}

module.exports = HelloWorld;